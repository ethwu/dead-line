# Dead/Line

## Attribution

This work is based on *Blades in the Dark* (found at http://www.bladesinthedark.com/),
product of One Seven Design, developed and authored by John Harper,
and licensed for use under the Creative Commons Attribution 3.0 Unported license (http://creativecommons.org/licenses/by/3.0/).

### Inspiration

- *Blades in the Dark*, by John Harper
- *Dragonriders of Pern*, by Anne McCaffrey
- *Girl by Moonlight*, by Andrew Gillis
- *CBR+PNK*, by Emanoel Melo
- *Thirsty Sword Lesbians*, by April Kit Walsh
- *Wicked Ones*, by Ben Nielson
