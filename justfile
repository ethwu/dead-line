
# Load a `.env` file if it exists.
set dotenv-load

# Use a texmf folder to contain shared configuration.
export TEXMFHOME := (
    justfile_directory() / 'lib/multidoc/texmf' +
    ',' + justfile_directory() / 'texmf' +
    if env('TEXMFHOME', '') != '' { ',' + env('TEXMFHOME') } else { '' }
)

# Directory containing TeX sources.
src := 'src'
# Build directory.
build := quote(justfile_directory() / 'build')
# Name of the main file.
main := 'main.tex'

# LaTeX engine.
latex := 'xelatex'
# OS file opener.
open := if os_family() == 'windows' { 'start' } else { 'open' }

# Build the final PDF and open it.
build-open file=main: (build-final file) (open file)

# Build the final PDF.
build-final file=main: (draft-pass file) (compile file '')

# Build the project without generating a PDF.
draft-pass file=main: (compile file '--no-pdf')

# Compile the given file.
[private]
compile file args:
    cd {{quote(src / parent_directory(file))}} && \
        {{latex}} {{args}} -output-directory={{build}} {{quote(file_name(file))}}

# Open the rendered PDF.
open file=main:
    {{open}} {{build / without_extension(file)}}.pdf
alias o := open

# Get the path to the rendered PDF.
output-path file=main:
    @echo {{quote(src / without_extension(file))}}.pdf

# Set up for development.
setup:
    git submodule update --init --recursive
    @printf 'XeLaTeX build...' ; command -v xelatex > /dev/null && echo 'OK' || echo 'Not OK'
    @printf 'Tectonic build...' ; command -v tectonic > /dev/null && echo 'OK' || echo 'Not OK'

# Build using Tectonic.
tectonic-build file=main:
    tectonic -X compile \
        $(fd -e cls -e sty '' -x printf -- '-Z search-path=%s\n' {//} | paste -s -) \
        {{quote(file)}}

# Clean up build artifacts.
clean:
    fd -I '.*\.(aux|glo|idx|ilg|ind|ist|log|out|pdf|sta|synctex.gz|xdv)' {{quote(src)}} -x rm {}
alias c := clean
