\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{sourcebook}[2023-04-03 TTRPG sourcebook.]

\RequirePackage{xparse}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{multidocument}}

\ProcessOptions*\relax

\LoadClass[
    baseclass=article,
]{multidocument}

\RequirePackage{etoolbox}
\AtEndPreamble{%
    % Load the referencing packages.
    \RequirePackage{refs}
}

\RequirePackage{fonts}

% Fancy headers.
\RequirePackage{header}

% Figures and tables.
\RequirePackage{booktabs}

\RequirePackage{imakeidx}
\makeindex[intoc, columns=2]

% hyperref fix for `memoir`
\RequirePackage{memhfixc}

% TItle of a book.
\NewDocumentCommand{\booktitle}{ m }{\emph{#1}}

% Name of the game system.
\NewDocumentCommand{\systemname}{}{System Name}
\NewDocumentCommand{\thissystem}{}{\booktitle{\systemname}}

\RequirePackage{fitd}
\RequirePackage{dead-line}

\endinput